---
layout: handbook-page-toc
title: "Customer Health Scores"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

---

**All health score information has been consolidated to the [Customer Health Assessment and Triage](/handbook/customer-success/tam/health-score-triage/) page.**