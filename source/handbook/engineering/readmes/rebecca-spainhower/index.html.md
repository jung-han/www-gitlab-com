---
layout: markdown_page
title: "Rebecca Spainhower's README"
job: "Support Engineering Manager"
---

### My Role and Responsibilities
For a quick summary of everything I feel is important as a Support Engineering Manager, please see [this article](https://circleci.com/blog/how-engineering-managers-can-effectively-support-engineers-teams-and-organizations/#) by [Lena Reinhard](https://www.linkedin.com/in/lenareinhard/), VP of Product Engineering at CircleCI. 

### Tips on Excellent Management
[A new manager's guide to growing into your role](https://www.atlassian.com/blog/leadership/new-manager-tips) -- from Atlassian blog on Leadership
