---
layout: handbook-page-toc
title: "RushTranslate"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Uses

RushTranslate is a SaaS translation services primarily used, owned, and operated by the content marketing team. 

## Ordering

When placing an order for translation, please have an accompanying issue link attached to the order in RushTranslate and copy/paste the order number from RushTranslate to your respective issue. 